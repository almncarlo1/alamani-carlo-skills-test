class Account():
    def __init__(self, id:str, name:str):
        self.id = id
        self.name = name

class StudentAccount(Account):
    def __init__(self, id:str, name:str):
        super().__init__(id, name)
        self.classes = []
        self.is_enlistment_locked = False
        self.is_enlisted = False
    
    def add_class(self, class_name:str):
        self.classes.append(class_name)

    def lock_enlistment(self):
        self.is_enlistment_locked = True
        print(f"{self.name} has locked enlistment.")

class AdviserAccount(Account):
    def __init__(self, id:str, name:str):
        super().__init__(id, name)
        self.advisees = []
        self.enlisted_advisees = []
    
    def add_advisee(self, advisee:Account):
        self.advisees.append(advisee)
        print(f"{self.name} has added {advisee.name} as an advisee.")

    def print_advisees(self):
        out = f"{self.name}'s advisees:"
        for advisee in self.advisees:
            out += f" {advisee.name}"
        print(out)

    def lock_enlistment_for(self, advisee:Account):
        if advisee not in self.advisees:
            print(f"ERROR: {advisee.name} is not an advisee of {self.name}.")
        elif advisee.is_enlistment_locked == False:
            print(f"ERROR: {advisee.name}'s enlistment is not locked yet.")
        else:
            advisee.is_enlisted = True
            print(f"{advisee.name} is now enlisted.")


if __name__ == "__main__":
    student1 = StudentAccount("05524", "Ross")
    student1.add_class("Class 1")
    student1.add_class("Class 2")
    student1.add_class("Class 4")
    student1.lock_enlistment()
    # Ross has locked enlistment.

    adviser = AdviserAccount("01341", "Rachel")
    adviser.add_advisee(student1)
    # Rachel has added Ross as an advisee.
    adviser.lock_enlistment_for(student1)
    # Ross is now enlisted.

    student2 = StudentAccount("12345", "Chandler")
    student2.add_class("Class 1")
    student2.add_class("Class 3")


    adviser.add_advisee(student2)
    # Rachel has added Chandler as an advisee.
    adviser.lock_enlistment_for(student2)
    # ERROR: Chandler's enlistment is not locked yet.

    student3 = StudentAccount("01353", "Joey")
    student3.add_class("Class 5")
    student3.add_class("Class 9")


    adviser.lock_enlistment_for(student3)
    # ERROR: Joey is not an advisee of Rachel.